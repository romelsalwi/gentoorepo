#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PS1="[\u@\h (\A) \w]\$ > "
#export PS1="[\[\e[36m\]\u\[\e[m\]@\[\e[31m\]\H\[\e[m\]] \[\e[37m\]\A\[\e[m\] \[\e[32m\]\W\[\e[m\] \[\e[33m\]\\$\[\e[m\] > "

export QT_STYLE_OVERRIDE=adwaita-dark

function nonzero_return() {
	RETVAL=$?
	[ $RETVAL -ne 0 ] && echo "$RETVAL"
}

export HISTCONTROL=ignoreboth:erasedups
export HISTIGNORE=" *:rm*:mv*:cd*:ls*:eix*:ddgr*:kill*:man*"

HISTSIZE=10000

# Line wrap on resize
shopt -s checkwinsize

# Causes bash to append to history instead of overwriting it so if you start a new terminal, you have old session history
shopt -s histappend
PROMPT_COMMAND='history -a'

# Bash completion
complete -cf doas #moody
complete -cf sudo

# Preserve root
alias chmod='chmod --preserve-root'

#COLOR alias
#alias ls='ls --color=always --group-directories-first --human-readable'
alias diff='diff --color=auto'
alias ip='ip -color=auto'
export LESS='-R --use-color' 
#LSD
alias lsd='lsd --total-size --group-directories-first --date=relative -alA'
alias eza='eza --icons --sort=name --header --group --group-directories-first'
alias ls='eza' #switching to eza

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
#alias cp='cp -i'
#alias mv='mv -i'
#alias rm='rm -i'

alias wine='runaswine'

#chroot Unto Gentoo
alias chrGen="mount --types proc /proc /mnt/gentoo/proc; mount --rbind /sys /mnt/gentoo/sys; mount --make-rslave /mnt/gentoo/sys; mount --rbind /dev /mnt/gentoo/dev; mount --make-rslave /mnt/gentoo/dev; mount --bind /run /mnt/gentoo/run; mount --make-slave /mnt/gentoo/run; chroot /mnt/gentoo /bin/bash"

# grub-mkconfig default
#alias grub-mk="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# Backup home directory
homeBackup() {
	rsync -aAXHvP --delete --info=stats1,progress2 --exclude={"/.cache","/.dbus","/.thumbnails","/pData"} /home/l3m0r/ "$@"
}
# Backup A to B
bakkupr() {
	rsync -aAXHvP --delete --info=stats1,progress2 "$@"
}

#Libvirt on OpenRC
alias iniLibvirt="sudo rc-service libvirtd start"

#Remove duplicates in history
unduphistory() {
	awk '{ seen[$0] = NR } END { for (k in seen) out[seen[k]] = "x" k; for (i = 1; i <= NR; ++i) if (out[i] != "") print substr(out[i], 2) }' < ~/.bash_history > ~/unduped_history && cp ~/unduped_history ~/.bash_history && rm ~/unduped_history; 
}

#rSync alternatives
cpr() {
  rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 "$@"
} 
mvr() {
  rsync --archive -hh --partial --info=stats1,progress2 --modify-window=1 --remove-source-files "$@"
}

#POWERLINE
#powerline-daemon -q
#POWERLINE_BASH_CONTINUATION=1
#POWERLINE_BASH_SELECT=1
#. /usr/lib/python3.10/site-packages/powerline/bindings/bash/powerline.sh

eval "$(starship init bash)"

#TOOL ALIASES

alias ncdu="ncdu --exclude=/media"
alias SS="maim -s -u ~/Pictures/SS/$(date +'%d-%m-%Y-%H%M%S').png"

# ddg on term search
if [ -f $HOME/scripts/ddgr-main/ddgr ]
then
	source $HOME/scripts/ddgr-main/auto-completion/bash/ddgr-completion.bash
else
	echo "ddgr-main not found"
fi

ddgr() {
	$HOME/scripts/ddgr-main/ddgr --unsafe "$@"
}

#z.sh
_Z_DATA=$HOME/.config/.z
_Z_NO_RESOLVE_SYMLINKS=1
. /home/l3m0r/scripts/z-1.12/z.sh
###

#FZF	###----	###----	###----	###
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export FZF_COMPLETION_TRIGGER='~~'

export FZF_DEFAULT_OPTS='-x --ansi --cycle --exact --info=inline --height=70% --border=none
	--preview-window=down,50%,border-none'

export FZF_CTRL_T_COMMAND="bfs -color -mindepth 1 -exclude -name .git -exclude -name .vim -printf '%P\n' 2>/dev/null"
export FZF_ALT_C_COMMAND="bfs -color -mindepth 1 -exclude -name .git -exclude -name .vim -type d -printf '%P\n' 2>/dev/null"
_fzf_compgen_path() {
    bfs "$1" -color -exclude -name .git -exclude -name .vim 2>/dev/null
}

_fzf_compgen_dir() {
    bfs "$1" -color -type d -exclude -name .git -exclude -name .vim 2>/dev/null
}

# Advanced customization of fzf options via _fzf_comprun function
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd|tree)		fzf --preview 'tree -C {} | head -200'   "$@" ;;
    export|unset)	fzf --preview "eval 'echo \$'{}"         "$@" ;;
    ssh)		fzf --preview 'drill {}'                 "$@" ;;
    *)			fzf --preview 'bat -n --color=always {}' "$@" ;;
  esac
}

# Preview file content using bat (https://github.com/sharkdp/bat)
export FZF_CTRL_T_OPTS="
	--preview 'bat -n --color=always {}'
	--bind 'ctrl-/:change-preview-window(down|hidden|)'"

# CTRL-/ to toggle small preview window to see the full command
# CTRL-Y to copy the command into clipboard using pbcopy
export FZF_CTRL_R_OPTS="
	--no-sort --exact
	--preview 'echo {}' --preview-window up:3:hidden:wrap
	--bind 'ctrl-/:toggle-preview'
	--bind 'ctrl-y:execute-silent(echo -n {2..} | pbcopy)+abort'
	--color header:italic
	--header 'Press CTRL-Y to copy command into clipboard'"

# Print tree structure in the preview window
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -100'"

_fzf_setup_completion path wgetpaste aria2c vim sudo 
_fzf_setup_completion dir tree cd rsync 

if [ -f $HOME/scripts/fzf_scripts.sh ]
then
	source $HOME/scripts/fzf_scripts.sh
else
	echo "fuzzy scripts not found"
fi
# END OF FZF section.	###----	###

# nnn Config
export NNN_PLUG='p:preview-tui;m:nmount;r:rsynccp;b:nbak;s:suedit;'
export NNN_FIFO=/tmp/nnn.fifo

if [ -f $HOME/scripts/quitcd.bash_sh_zsh ]
then
	source $HOME/scripts/quitcd.bash_sh_zsh
else
	echo "quitch.bash_sh scripts not found"
fi
# END OF nnn section.
