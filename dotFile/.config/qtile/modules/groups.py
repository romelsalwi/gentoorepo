from libqtile.config import Group, Match
from .keys import *
from .layouts import layouts

groups = [
    Group("", layout="monadtall",	    #Web
		  matches=[Match(wm_class=["librewolf","firefox","Firefox Beta","chromium","Tor Browser"])]),
    Group("", layout="max",	        #VMs
		  matches=[Match(wm_class=["KeePassXC","virt-manager","Daedalus Mainnet","Sublime_text","FreeTube"])]),
    Group("", layout="monadtall",      #Code, Dev
		  matches=[Match(wm_class=["Notepadqq","nitrogen","Gvim","obsidian"])]),
    Group("", layout="columns",        #Files
		  matches=[Match(wm_class=["Thunar","ristretto","Spacefm"])]),
    Group("", layout="columns",        #Social apps
		  matches=[Match(wm_class=["telegram-desktop","discord","Session","Signal","element","schildichat","Gajim","nheko"]), Match(title=["newsboat"])]),
    Group("", layout="monadtall",  	#Utilities-1
		 matches=[Match(wm_class=["Vncviewer","Veracrypt","liferea"]),
				 Match(title=[])]
         ),
    Group("", layout="max",    		#AV
		  matches=[Match(wm_class=["spotify", "clementine", "kdenlive", "audacious","Wireshark","Psensor","GParted","Hexchat"]),
				   Match(title=["VLC media player","GNU Image Manipulation Program"])]),
    Group("", layout="columns",		#Utilities-2
		  matches=[Match(title=["Filelight","Turtl","BleachBit","Network Connections","Timeshift"]),
				  Match(wm_class=["Firewall-config","lightdm-gtk-greeter-settings","Gufw.py","Nm-connection-editor","Pavucontrol","elogviewer"])]
		 )
	]

for k, group in zip(["1", "2", "3", "4", "5", "6", "7", "8"], groups):
    keys.append(Key("M-"+(k), lazy.group[group.name].toscreen()))
    keys.append(Key("M-S-"+(k), lazy.window.togroup(group.name,switch_group=True)))
