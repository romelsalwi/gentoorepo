from libqtile import layout
from libqtile.config import Match
from .widgets import colors

def init_layout_theme():
    return {
            "margin": 0,
            "border_width": 1,
            "border_focus": colors[16],
            "border_normal": '#4c566a',
			"grow_amount": 2,
            "change_size": 2,
            }

layout_theme = init_layout_theme()

layouts = [
    layout.Max(**layout_theme),
	layout.Columns(**layout_theme),
	#layout.Stack(num_stacks=2, **layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Matrix(**layout_theme),
    layout.MonadTall(**layout_theme),
    #layout.MonadWide(**layout_theme),
    
    #layout.Tile(**layout_theme),
    #layout.TreeTab(
    #    sections=['FIRST', 'SECOND'],
    #    bg_color='#3b4252',
    #    active_bg='#bf616a',
    #    inactive_bg='#a3be8c',
    #    padding_y=5,
    #    section_top=10,
    #    panel_width=280
    #),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),		# gitk
    Match(wm_class='makebranch'),		# gitk
    Match(wm_class='maketag'),			# gitk
    Match(wm_class='ssh-askpass'),		# ssh-askpass
    Match(title='branchdialog'),		# gitk
    Match(title='pinentry'),			# GPG key password entry
	Match(title='New bookmark'),		#turtl
	Match(title='Search'),
    ],
    border_focus=colors[12] [0]
)
