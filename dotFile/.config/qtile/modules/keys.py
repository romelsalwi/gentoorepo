from libqtile.config import EzKey as Key
from .mouse import *

terminal = "kitty"

keys = [
    # Switch between windows
    Key("M-<Left>",			lazy.layout.left(),					desc="Move focus to left"),
    Key("M-<Right>",		lazy.layout.right(),				desc="Move focus to right"),
    Key("M-<Down>",			lazy.layout.down(),					desc="Move focus down"),
    Key("M-<Up>",			lazy.layout.up(),					desc="Move focus up"),
    Key("M-<Tab>",			lazy.layout.next(),					desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key("M-S-<Left>",		lazy.layout.shuffle_left(),			desc="Move window to the left"),
    Key("M-S-<Right>",		lazy.layout.shuffle_right(),		desc="Move window to the right"),
    Key("M-S-<Down>",		lazy.layout.shuffle_down(),			desc="Move window down"),
    Key("M-S-<Up>",			lazy.layout.shuffle_up(),			desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key("M-C-<Left>",		lazy.layout.grow_left(),			desc="Grow window to the left"),
    Key("M-C-<Right>",		lazy.layout.grow_right(),			desc="Grow window to the right"),
    Key("M-C-<Down>",		lazy.layout.shrink(),   			desc="Grow window down"),
    Key("M-C-<Up>",			lazy.layout.grow(),		    		desc="Grow window up"),
    Key("M-n",				lazy.layout.normalize(),			desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key("M-<space>",		lazy.next_layout(),					desc="Toggle between split and unsplit sides of stack"),
	Key("M-<Return>",		lazy.spawn(terminal),				desc="Launch terminal"),
	Key("M-l",      		lazy.spawn("slock"),				desc="Lock it!"),

    # Toggle between different layouts as defined below
    Key("M-f",				lazy.window.toggle_fullscreen(),	desc="Toggle between layouts"),
    Key("M-w",				lazy.window.kill(),					desc="Kill focused window"),
    Key("M-C-r",			lazy.restart(),						desc="Restart Qtile"),
    Key("M-C-q",			lazy.shutdown(),					desc="Shutdown Qtile"),
    Key("A-r",				lazy.spawncmd(),					desc="Spawn a command using a prompt widget"),
	Key("M-e",				lazy.spawn("rofi -show drun")),
	Key("M-c",				lazy.spawn("rofi -show calc -modi calc -no-show-match -no-sort")),
	Key("A-<Tab>",			lazy.spawn("rofi -show window")),
	Key("M-r",				lazy.spawn("rofi -show run")),
	Key("<Print>",			lazy.spawn(["sh", "-c", "maim -s ~/Pictures/SS/$(date +'%d-%m-%Y-%H%M%S').png"])),

    # Keybindings to launch user defined programs
    Key("A-e",				lazy.spawn("spacefm")),
    Key("A-t",				lazy.spawn("kitty -e s-tui")),
    Key("A-S-e",			lazy.spawn("kitty -e vifm")),
    Key("A-b",				lazy.spawn("kitty -e btop")),
    Key("A-i",				lazy.spawn("kitty -e iftop")),
    Key("A-v",				lazy.spawn("mpv --player-operation-mode=pseudo-gui")),
    Key("A-1",				lazy.spawn("librewolf -P seek")),
    Key("A-2",				lazy.spawn("librewolf -P tor")),
	Key("A-3",				lazy.spawn("./Apps/Obsidian-1.5.3.AppImage")),
	Key("A-4",				lazy.spawn("element-desktop")),
	Key("A-5",				lazy.spawn("gajim")),
	Key("A-6",				lazy.spawn("Apps/liftoff-v0.10.10-x86_64-linux/Liftoff")),
	Key("A-7",				lazy.spawn("./Apps/freetube_0.19.1_amd64.AppImage")),
	Key("A-8",				lazy.spawn("./Apps/kdenlive-23.04.2-x86_64.AppImage")),

]
