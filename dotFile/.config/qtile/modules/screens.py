from libqtile import bar
from .widgets import *
from libqtile.config import Screen

import os

#---MOUSE CALLBACKS---

def open_btop():
    qtile.cmd_spawn('kitty -e btop')

def open_onboard():
    qtile.cmd_spawn('onboard')

def open_firefox():
    qtile.cmd_spawn('librewolf')
	
def open_session():
	qtile.cmd_spawn('./Applications/session-desktop-linux-x86_64-1.8.6.AppImage')
	
def open_obsidian():
	qtile.cmd_spawn('./Applications/Obsidian-0.15.8.AppImage')

def open_gimp():
    qtile.cmd_spawn('gimp --show-playground')

def open_kdenlive():
    qtile.cmd_spawn('kdenlive')

def open_mpv():
    qtile.cmd_spawn('mpv --player-operation-mode=pseudo-gui')
	
def open_nvtop():
    qtile.cmd_spawn('kitty -e nvtop')

def open_iftop():
    qtile.cmd_spawn('kitty -e iftop')

def open_top():
    qtile.cmd_spawn('kitty -e top')

def open_atop():
    qtile.cmd_spawn('kitty -e atop')
	
def open_iotop():
    qtile.cmd_spawn('kitty -e iotop')

#------

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    background=colors[1], #2e3440
                    foreground=colors[5], #d8dee9
                    linewidth=1,
                    padding=10
                ),
                widget.Image(
                    filename="~/.config/qtile/icons/qtilelogo.png",
                    iconsize=8,
                    background=colors[1],
                    mouse_callbacks={'Button1': lambda : qtile.cmd_spawn('rofi -show combi')}
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.GroupBox(
                    active=colors[16], #b48ead
                    borderwidth=2,
                    disable_drag=True,
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    hide_unused=False,
                    highlight_method='line',
					inactive=colors[4], #e5e9f0
                    margin_x=0,
                    margin_y=3,
                    padding_x=5,
                    padding_y=8,
                    rounded=False,
                    this_current_screen_border=colors[14], #ebcb8b
                    urgent_alert_method='line'
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.CurrentLayoutIcon(
                    background=colors[1],
                    custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
                    foreground=colors[6], #e5e9f0
                    padding=0,
                    scale=0.65
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.CurrentLayout(
                    background=colors[1],
                    font='Ubuntu Bold',
                    foreground=colors[6]
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
				#---SHORTCUTS---
				widget.TextBox(fmt=' ',background=colors[1],foreground=colors[6],fontsize=12,mouse_callbacks={ 'Button1':open_mpv }),
                widget.TextBox(fmt=' ',background=colors[1],foreground=colors[6],fontsize=12,mouse_callbacks={ 'Button1':open_firefox }),
                widget.TextBox(fmt=' ',background=colors[1],foreground=colors[6],fontsize=12,mouse_callbacks={ 'Button1':open_atop }),
				
				widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
				#------
				
                widget.Prompt(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[6]
                ),
                widget.Spacer(),
				
				#---buildit Notifier---
				widget.Notify(),
				widget.Sep(
                    background=colors[1],
                    foreground=colors[4],
                    linewidth=1,
                    padding=10
                ),
				#---
				
                widget.TextBox(
                    background=colors[1],
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    foreground=colors[6],
                    padding=0,
                    text=' ',
					mouse_callbacks={'Button1': open_onboard}
                ),
                widget.KeyboardLayout(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[6]
                ),
                widget.CapsNumLockIndicator(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[6]
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[4],
                    linewidth=1,
                    padding=10
                ),
                widget.TextBox(
                    background=colors[1],
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    foreground=colors[6],
                    padding=0,
                    text=' '
                ),
                widget.Clock(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[6],
                    format='%a %d, %B %H:%M '
                ),
            ],
            20,
            opacity=1.00
        ),
        bottom=bar.Bar(
            [
                widget.WindowName(
                    background=colors[1],
                    foreground=colors[6],
                    font='Ubuntu',
                    fontsize = 12,
                    max_chars=60
                ),
                widget.Spacer(),
				
				widget.Systray(
                    background=colors[1],
                    icon_size=20,
                    padding=4
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.TextBox(
                    background=colors[1],
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    foreground=colors[6],
                    padding=0,
                    text=' '
                ),
                widget.ThermalSensor(
                    background=colors[1],
                    font='Ubuntu',
					tag_sensor="Tctl",
                    fontsize=12,
                    foreground=colors[13],
                    update_interval=2
                ),
				widget.NvidiaSensors(
					background=colors[1],
                    font='Ubuntu',
					fontsize=12,
                    foreground=colors[15],
                    update_interval=2,
					mouse_callbacks={'Button1': open_nvtop}
				),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.TextBox(
                    background=colors[1],
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    foreground=colors[6],
                    padding=0,
                    text=' '
                ),
                widget.Memory(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[6],
                    format="{MemUsed: .0f}{mm}",
                    update_interval=1.0,
					mouse_callbacks={'Button1': open_btop}
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.TextBox(
                    background=colors[1],
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    foreground=colors[6],
                    padding=0,
                    text=' '
                ),
                widget.CPU(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[6],
                    format='CPU {load_percent}%',
                    update_interval=1,
					mouse_callbacks={'Button1': open_top}
                ),
                widget.Sep(
                    background=colors[1],
                    foreground=colors[5],
                    linewidth=1,
                    padding=10
                ),
                widget.TextBox(
                    background=colors[1],
                    font='Ubuntu Nerd Font',
                    fontsize=14,
                    foreground=colors[6],
                    padding=0,
                    text='  '
                ),
                widget.Net(
                    background=colors[1],
                    font='Ubuntu',
                    fontsize=12,
                    foreground=colors[5],
                    format='  {down:.2f}{down_suffix}   {up:.2f}{up_suffix}',
                    interface='enp37s0',
					prefix='M',
                    padding=0,
                    mouse_callbacks={'Button1': open_iftop}
                ),
                #widget.NetGraph(
                #    background=colors[1],
                #    bandwidth="down",
                #    border_color=colors[5],
                #    border_width=0,
                #    fill_color=colors[9], #88c0d0
                #    foreground=colors[5],
                #    graph_color=colors[9],
                #    interface="auto",
                #    line_width=1,
                #    padding=0,
                #    type='linefill'
                #),
            ],
            20,
            opacity=1.00
        ),
    ),
]
