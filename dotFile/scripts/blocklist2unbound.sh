#! /bin/bash

microsoftBlocklistURL=${1:-"https://raw.githubusercontent.com/jmdugan/blocklists/master/corporations/microsoft/all"}
anudeepBlocklist=${1:-"https://hosts.anudeep.me/mirror/adservers.txt"}
facebookBlocklist=${1:-"https://raw.githubusercontent.com/jmdugan/blocklists/master/corporations/facebook/all"}

EndpointIP=${2:-"0.0.0.0"}
GeneratedOutput=${3:-"blockthese.conf"}

wget -qO- ${microsoftBlocklistURL} | grep '^0\.0\.0\.0' | awk '{print "local-zone: \""$2"\" always_refuse"}' >> ${GeneratedOutput};
wget -qO- ${anudeepBlocklist} | grep '^0\.0\.0\.0' | awk '{print "local-zone: \""$2"\" always_refuse"}' >> ${GeneratedOutput};
wget -qO- ${facebookBlocklist} | grep '^0\.0\.0\.0' | awk '{print "local-zone: \""$2"\" always_refuse"}' >> ${GeneratedOutput};
